package com.khushbooraheja.geofencingdemo;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.Toast;
import com.google.android.gms.location.GeofencingClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.khushbooraheja.geofencingdemo.list_utils.PlaceData;
import com.khushbooraheja.geofencingdemo.list_utils.PlacesAdapter;
import com.khushbooraheja.geofencingdemo.list_utils.PlacesView;
import java.util.List;

public class GeofencePlacesActivity extends AppCompatActivity implements PlacesView {

  RecyclerView mGeofencePlacesList;
  PlacesAdapter adapter;
  private GeofencingClient mGeofencingClient;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_geofence_places);

    mGeofencingClient = LocationServices.getGeofencingClient(this);

    mGeofencePlacesList = (RecyclerView) findViewById(R.id.geofence_places_list);
    RecyclerView.LayoutManager mLayoutManager = new
        LinearLayoutManager(getApplicationContext());
    mGeofencePlacesList.setLayoutManager(mLayoutManager);

    adapter = new PlacesAdapter(this, getList(), this);
    mGeofencePlacesList.setAdapter(adapter);

  }

  private List<PlaceData> getList() {
    List<PlaceData> list = null;
    list = PreferenceUtils.getAllPlaces(getApplicationContext());
    return list;
  }

  @Override
  public void onRemoveClick(String prefKeyToRemove, List<String> geofenceRequestIDs) {
    PreferenceUtils.removeSharedSetting(this, prefKeyToRemove);
    adapter.notifyPlacesListChanged(getList());
    mGeofencingClient.removeGeofences(geofenceRequestIDs)
        .addOnCompleteListener(new OnCompleteListener<Void>() {
          @Override
          public void onComplete(
              @NonNull
                  Task<Void> task) {
            if (task.isSuccessful()) {
              Toast.makeText(GeofencePlacesActivity.this,
                  "Geofence removed successfully.", Toast.LENGTH_SHORT).show();
            } else {
              Toast.makeText(GeofencePlacesActivity.this,
                  "Geofence removal unsuccessful.", Toast.LENGTH_SHORT).show();
            }
          }
        });
  }
}
