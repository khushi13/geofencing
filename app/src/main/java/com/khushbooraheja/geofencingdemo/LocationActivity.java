package com.khushbooraheja.geofencingdemo;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.PendingIntent;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.os.Bundle;
import android.os.IBinder;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.Toast;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.Geofence;
import com.google.android.gms.location.GeofencingClient;
import com.google.android.gms.location.GeofencingRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.places.AutocompleteFilter;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlaceAutocompleteFragment;
import com.google.android.gms.location.places.ui.PlaceSelectionListener;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;

public class LocationActivity extends AppCompatActivity
    implements OnMapReadyCallback, GoogleMap.OnMarkerClickListener {

  private static final String TAG = LocationActivity.class.getSimpleName();
  private static final int LOC_PERM_REQ_CODE = 1;
  //meters
  private static final int GEOFENCE_RADIUS = 50;
  //in milli seconds
  private static final int GEOFENCE_EXPIRATION = 6000;

  private GoogleMap mMap;

  private GeofencingClient mGeofencingClient;

  private double mMarkedLat, mMarkedLong;

  private ServiceConnection mServiceConnection;

  private Context mApplicationContext;
  private GeofenceReceiver mGeofenceReceiver;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_location);

    mApplicationContext = getApplicationContext();

    Toolbar tb = findViewById(R.id.toolbar);
    setSupportActionBar(tb);
    tb.setSubtitle("Location Alert");

    SupportMapFragment mapFragment =
        (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.g_map);
    mapFragment.getMapAsync(this);

    mGeofencingClient = LocationServices.getGeofencingClient(this);

    PlaceAutocompleteFragment autocompleteFragment =
        (PlaceAutocompleteFragment) getFragmentManager().findFragmentById(
            R.id.place_autocomplete_fragment);

    autocompleteFragment.setOnPlaceSelectedListener(new PlaceSelectionListener() {
      @Override
      public void onPlaceSelected(Place place) {
        // TODO: Get info about the selected place.
        Log.i(TAG, "Place: " + place.getName());
        mMarkedLat = place.getLatLng().latitude;
        mMarkedLong = place.getLatLng().longitude;

        Toast.makeText(LocationActivity.this, "loc:" + mMarkedLat + "," + mMarkedLong,
            Toast.LENGTH_SHORT).show();
        showLocationOnMap(mMarkedLat, mMarkedLong, place.getName().toString(),
            place.getAddress().toString());
        /*
        mMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
          @Override
          public boolean onMarkerClick(Marker marker) {
            Toast.makeText(LocationActivity.this, "onMarkerClick", Toast.LENGTH_SHORT).show();
            return false;
          }
        });
        */
      }

      @Override
      public void onError(Status status) {
        // TODO: Handle the error.
        Log.i(TAG, "An error occurred: " + status);
      }
    });

    AutocompleteFilter typeFilter =
        new AutocompleteFilter.Builder().setTypeFilter(AutocompleteFilter.TYPE_FILTER_NONE).build();
    autocompleteFragment.setFilter(typeFilter);

    mServiceConnection = new ServiceConnection() {
      @Override
      public void onServiceConnected(ComponentName name, IBinder service) {
        //locationService = ((GetLocationService.GeoBinder) service).getLocationService();
      }

      @Override
      public void onServiceDisconnected(ComponentName name) {
        //locationService=null;
      }
    };
    mGeofenceReceiver = new GeofenceReceiver();
  }

  @Override
  public void onMapReady(GoogleMap googleMap) {
    Log.d(TAG, "onMapReady:");
    mMap = googleMap;

    mMap.getUiSettings().setZoomControlsEnabled(true);
    mMap.setMinZoomPreference(15);

    showCurrentLocationOnMap();

    /*
    mMap.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
      @Override
      public void onMapClick(LatLng latLng) {
        addLocationAlert(latLng.latitude, latLng.longitude);
        Log.d("latLongK", "lat:"+latLng.latitude+" ==long:"+latLng.longitude);
      }
    });
    */
  }

  @SuppressLint("MissingPermission")
  private void showCurrentLocationOnMap() {
    if (isLocationAccessPermitted()) {
      requestLocationAccessPermission();
    } else if (mMap != null) {
      mMap.setMyLocationEnabled(true);
    }
  }

  private boolean isLocationAccessPermitted() {
    if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
        != PackageManager.PERMISSION_GRANTED) {
      return true;
    } else {
      return false;
    }
  }

  private void requestLocationAccessPermission() {
    ActivityCompat.requestPermissions(this,
        new String[] { Manifest.permission.ACCESS_FINE_LOCATION }, LOC_PERM_REQ_CODE);
  }

  @SuppressLint("MissingPermission")
  private void addLocationAlert(double lat, double lng) {
    if (isLocationAccessPermitted()) {
      requestLocationAccessPermission();
    } else {
      String key = "" + lat + "-" + lng;
      Geofence geofence = getGeofence(lat, lng, key);
      mGeofencingClient.addGeofences(getGeofencingRequest(geofence), getGeofencePendingIntent())
          .addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(
                @NonNull
                    Task<Void> task) {
              if (task.isSuccessful()) {
                Toast.makeText(LocationActivity.this, "Location alter has been added",
                    Toast.LENGTH_SHORT).show();
              } else {
                Toast.makeText(LocationActivity.this, "Location alter could not be added",
                    Toast.LENGTH_SHORT).show();
              }
            }
          });
    }
  }

  private void removeLocationAlert() {
    if (isLocationAccessPermitted()) {
      requestLocationAccessPermission();
    } else {
      mGeofencingClient.removeGeofences(getGeofencePendingIntent())
          .addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(
                @NonNull
                    Task<Void> task) {
              if (task.isSuccessful()) {
                Toast.makeText(LocationActivity.this, "Location alters have been removed",
                    Toast.LENGTH_SHORT).show();
              } else {
                Toast.makeText(LocationActivity.this, "Location alters could not be removed",
                    Toast.LENGTH_SHORT).show();
              }
            }
          });
    }
  }
  @Override
  protected void onStart() {
    super.onStart();
    IntentFilter filter=new IntentFilter();
    filter.addAction("com.ad4screen.sdk.intent.action.TRIGGER");
    filter.addCategory("com.ad4screen.sdk.intent.category.GEOFENCE_NOTIFICATIONS");
    registerReceiver(mGeofenceReceiver,filter);
  }

  @Override
  public void onRequestPermissionsResult(int requestCode,
      @NonNull
          String[] permissions,
      @NonNull
          int[] grantResults) {
    switch (requestCode) {
      case LOC_PERM_REQ_CODE: {
        if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
          showCurrentLocationOnMap();
          Toast.makeText(LocationActivity.this,
              "Location access permission granted, you try " + "add or remove location allerts",
              Toast.LENGTH_SHORT).show();
        }
        return;
      }
    }
  }

  /*
  private PendingIntent getGeofencePendingIntent() {
    Intent intent = new Intent(this, LocationAlertIntentService.class);
    return PendingIntent.getService(this, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
  }
  */
  private PendingIntent getGeofencePendingIntent() {
    Intent intent = new Intent(this, GeofenceReceiver.class);
    // We use FLAG_UPDATE_CURRENT so that we get the same pending intent back when calling
    // addGeofences() and removeGeofences().
    return PendingIntent.getBroadcast(this, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
  }

  private GeofencingRequest getGeofencingRequest(Geofence geofence) {
    GeofencingRequest.Builder builder = new GeofencingRequest.Builder();

    builder.setInitialTrigger(GeofencingRequest.INITIAL_TRIGGER_DWELL);
    builder.addGeofence(geofence);
    return builder.build();
  }

  private Geofence getGeofence(double lat, double lang, String key) {
    return new Geofence.Builder().setRequestId(key)
        .setCircularRegion(lat, lang, GEOFENCE_RADIUS)
        .setExpirationDuration(Geofence.NEVER_EXPIRE)
        .setTransitionTypes(Geofence.GEOFENCE_TRANSITION_ENTER |
            Geofence.GEOFENCE_TRANSITION_DWELL |
            Geofence.GEOFENCE_TRANSITION_EXIT)
        .setLoiteringDelay(10000)
        .build();
  }

  @Override
  public boolean onCreateOptionsMenu(Menu menu) {
    MenuInflater inflater = getMenuInflater();
    inflater.inflate(R.menu.menu, menu);
    return true;
  }

  @Override
  public boolean onOptionsItemSelected(MenuItem item) {
    switch (item.getItemId()) {
      case R.id.remove_loc_alert:
        Log.d(TAG, "onOptionsItemSelected: " + mGeofencingClient);
        //removeLocationAlert();
        startActivity(new Intent(this, GeofencePlacesActivity.class));
        return true;
      default:
        return super.onOptionsItemSelected(item);
    }
  }

  private void showLocationOnMap(double latitude, double longitude, String placeName,
      String placeAddr) {
    LatLng position = new LatLng(latitude, longitude);

    //mMap.clear();

    MarkerOptions options = new MarkerOptions();
    options.position(position);
    options.title(placeName);
    options.snippet(placeAddr);

    mMap.addMarker(options);

    CameraPosition cameraPosition = new CameraPosition.Builder().target(position).zoom(4).build();
    CameraUpdate updatePosition = CameraUpdateFactory.newCameraPosition(cameraPosition);
    mMap.animateCamera(updatePosition);

    mMap.setOnMarkerClickListener(LocationActivity.this);
  }

  @Override
  public boolean onMarkerClick(Marker marker) {
    Log.d(TAG, "onMarkerClick: id:" + marker.getId());
    Log.d(TAG, "onMarkerClick: title:" + marker.getTitle());
    Log.d(TAG, "onMarkerClick: tag:" + marker.getTag());
    Log.d(TAG, "onMarkerClick: position:" + marker.getPosition());

    if (!PreferenceUtils.isContain(mApplicationContext, marker.getTitle())) {
      addLocationAlert(marker.getPosition().latitude, marker.getPosition().longitude);
      CircleOptions circleOptions = new CircleOptions().center(marker.getPosition())
          .radius(GEOFENCE_RADIUS)
          .fillColor(0x40ff0000)
          .strokeColor(Color.TRANSPARENT)
          .strokeWidth(2);
      mMap.addCircle(circleOptions);

      PreferenceUtils.saveSharedSetting(getApplicationContext(), marker.getTitle(),
          marker.getPosition());
      if (mServiceConnection != null) {
        //startService(new Intent(getApplicationContext(), GetLocationService.class));
      }
    } else {
      Toast.makeText(this, "Already added!", Toast.LENGTH_SHORT).show();
      String key = "" + marker.getPosition().latitude + "-" + marker.getPosition().longitude;
      Geofence geofence =
          getGeofence(marker.getPosition().latitude, marker.getPosition().longitude, key);
      Log.d(TAG, "onMarkerClick:request id " + geofence.getRequestId());
    }

    return false;
  }
}
