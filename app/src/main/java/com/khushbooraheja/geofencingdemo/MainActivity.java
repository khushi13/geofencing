package com.khushbooraheja.geofencingdemo;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_main);

    ((Button) findViewById(R.id.goToMap)).setOnClickListener(v -> {
      startActivity(new Intent(this, LocationActivity.class));
    });
  }
}
