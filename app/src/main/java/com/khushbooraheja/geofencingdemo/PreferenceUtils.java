package com.khushbooraheja.geofencingdemo;

import android.content.Context;
import android.content.SharedPreferences;
import android.text.TextUtils;
import android.util.Log;
import com.google.android.gms.maps.model.LatLng;
import com.google.gson.Gson;
import com.khushbooraheja.geofencingdemo.list_utils.PlaceData;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class PreferenceUtils {

  public static final String PREFERENCES_FILE = "GeoFence";

  public static void saveSharedSetting(Context context, String settingName, LatLng object) {
    SharedPreferences sharedPref =
        context.getSharedPreferences(PREFERENCES_FILE, Context.MODE_PRIVATE);
    SharedPreferences.Editor editor = sharedPref.edit();
    Gson gson = new Gson();
    String json = gson.toJson(object);
    editor.putString(settingName, json);
    editor.commit();
    editor.apply();
  }

  public static LatLng readSharedSetting(Context context, String settingName) {
    SharedPreferences sharedPref =
        context.getSharedPreferences(PREFERENCES_FILE, Context.MODE_PRIVATE);
    Gson gson = new Gson();
    String json = sharedPref.getString(settingName, "");
    LatLng obj = (LatLng) gson.fromJson(json, LatLng.class);
    return obj;
  }

  public static void removeSharedSetting(Context context, String settingName) {
    SharedPreferences sharedPref = context.getSharedPreferences(PREFERENCES_FILE,
        Context.MODE_PRIVATE);
    SharedPreferences.Editor editor = sharedPref.edit();
    editor.remove(settingName);
    editor.apply();
  }

  public static boolean isContain(Context context, String key) {
    if (TextUtils.isEmpty(key)) return false;
    return context.getSharedPreferences(PreferenceUtils.PREFERENCES_FILE, Context.MODE_PRIVATE)
        .contains(key);
  }

  public static List<PlaceData> getAllPlaces(Context context) {
    SharedPreferences sharedPref = context.getSharedPreferences(PREFERENCES_FILE,
        Context.MODE_PRIVATE);
    Map<String, ?> allEntries = sharedPref.getAll();
    List<PlaceData> list = new ArrayList<>();
    for (Map.Entry<String, ?> entry : allEntries.entrySet()) {
      Log.d("map values", entry.getKey() + ": " + entry.getValue().toString());
      list.add(new PlaceData(entry.getKey(), readSharedSetting(context, entry.getKey())));
    }
    return list;
  }
}
