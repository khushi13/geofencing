package com.khushbooraheja.geofencingdemo.list_utils;

import com.google.android.gms.maps.model.LatLng;

public class PlaceData {
  String placeTitle;
  LatLng latLng;

  public PlaceData(String placeTitle, LatLng latLng) {
    this.placeTitle = placeTitle;
    this.latLng = latLng;
  }

  public String getPlaceTitle() {
    return placeTitle;
  }

  public void setPlaceTitle(String placeTitle) {
    this.placeTitle = placeTitle;
  }

  public LatLng getLatLng() {
    return latLng;
  }

  public void setLatLng(LatLng latLng) {
    this.latLng = latLng;
  }
}
