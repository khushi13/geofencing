package com.khushbooraheja.geofencingdemo.list_utils;

import java.util.List;

public interface PlacesView {
  void onRemoveClick(String prefKeyToRemove, List<String> geofenceRequestIDs);
}
