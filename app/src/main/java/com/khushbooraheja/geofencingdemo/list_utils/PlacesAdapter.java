package com.khushbooraheja.geofencingdemo.list_utils;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import com.google.android.gms.maps.model.LatLng;
import com.khushbooraheja.geofencingdemo.R;
import java.util.ArrayList;
import java.util.List;

public class PlacesAdapter extends RecyclerView.Adapter<PlacesAdapter.PlaceHolder>{

  private Context context;
  private List<PlaceData> placeDataArrayList;
  private PlacesView placesView;

  public PlacesAdapter(Context context, List<PlaceData> placeDataArrayList,
      PlacesView placesView) {
    this.context = context;
    this.placeDataArrayList = placeDataArrayList;
    this.placesView = placesView;
  }

  @Override
  public PlaceHolder onCreateViewHolder(ViewGroup parent, int viewType) {
    View view = LayoutInflater.from(context)
        .inflate(R.layout.places_item, parent,false);
    return new PlaceHolder(view);
  }

  @Override
  public void onBindViewHolder(PlaceHolder holder, int position) {
    holder.placeTitle.setText(placeDataArrayList.get(position).placeTitle);
    holder.placeLatLong.setText(placeDataArrayList.get(position).latLng.toString());
    holder.removePlace.setOnClickListener(v ->
        placesView.onRemoveClick(placeDataArrayList.get(position).placeTitle,
            getGeofenceRequestId(placeDataArrayList.get(position).latLng)));
  }

  private List<String> getGeofenceRequestId(LatLng latLng) {
    double latitude = latLng.latitude;
    double longitude = latLng.longitude;
    String key = "" + latitude + "-" + longitude;
    List<String> geofenceRequestId = new ArrayList<>();
    geofenceRequestId.add(key);
    return geofenceRequestId;
  }

  public void notifyPlacesListChanged(List<PlaceData> newList) {
    this.placeDataArrayList = newList;
    notifyDataSetChanged();
  }

  @Override
  public int getItemCount() {
    return placeDataArrayList.size();
  }

  class PlaceHolder extends RecyclerView.ViewHolder {

    TextView placeTitle, placeLatLong;
    Button removePlace;

    PlaceHolder(View itemView) {
      super(itemView);
      placeTitle = (TextView) itemView.findViewById(R.id.place_title);
      placeLatLong = (TextView) itemView.findViewById(R.id.place_lat_long);
      removePlace = (Button) itemView.findViewById(R.id.remove_place);
    }

  }
}
// 28.591219199999994//77.31900879999999